import { FaAlignCenter, FaAlignJustify, FaAlignLeft, FaAlignRight, FaBold, FaFont, FaItalic, FaUnderline } from "react-icons/fa"
import { MdFormatSize } from "react-icons/md"

export const RichTextEditor = () => {
    return(
        <main className="w-full border-solid h-60 rounded bg-zinc-600 px-3 pb-2">

            <nav className="w-full h-10 flex flex-col justify-between align-middle py-2 px-2">
                <div className="flex justify-evenly align-middle w-full">
                    <div className="flex justify-evenly align-middle w-1/2">
                        <button title="Bold" className="text-sm hover:text-zinc-900 transition-all">
                            <FaBold />
                        </button>
                        <button title="Italic" className="text-sm hover:text-zinc-900 transition-all">
                            <FaItalic />
                        </button>
                        <button title="Underline" className="text-sm hover:text-zinc-900 transition-all">
                            <FaUnderline />
                        </button>
                        <button title="Text size" className="text-sm hover:text-zinc-900 transition-all">
                            <MdFormatSize className="text-base" />
                        </button>
                    </div>
                    
                    <div className="h-full bg-slate-200/30 w-px"></div>
                    
                    <div className="flex justify-evenly align-middle w-1/2">
                        <button title="Text align center" className="text-sm hover:text-zinc-900 transition-all">
                            <FaAlignCenter />
                        </button>
                        <button title="Text align justify" className="text-sm hover:text-zinc-900 transition-all">
                            <FaAlignJustify />
                        </button>
                        <button title="Text align left" className="text-sm hover:text-zinc-900 transition-all">
                            <FaAlignLeft />
                        </button>
                        <button title="Text align right" className="text-sm hover:text-zinc-900 transition-all">
                            <FaAlignRight />
                        </button>
                    </div>
                    
                </div>

                <div className="w-full bg-slate-200/30 h-px"></div>
            </nav>

            <textarea id="text-area-article" name="text-area-article" placeholder="Write your article content here..." className="w-full h-5/6 resize-none bg-transparent outline-none"></textarea>
       
        </main>
    )
}
