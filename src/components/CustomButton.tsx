import Link from "next/link";

type IProps  = {
    linkTo: string
    text: string
    type: 'primary' | 'secondary'
    isWidthFull: boolean
    isDisabled?: boolean
}

export const CustomButtom = ({linkTo, type, text, isWidthFull, isDisabled}: IProps) => {
    if(type === 'primary') {
        return(
            isWidthFull ?
            <Link href={linkTo}>
                <button disabled={isDisabled} className="bg-green-700 mb-4 font-semibold text-xl disabled:bg-slate-400 disabled:text-zinc-700 disabled:cursor-auto rounded w-full py-3 text-white cursor-pointer hover:bg-green-600 transition">
                    {text}
                </button>
            </Link>
            : 
            <Link href={linkTo}>
                <button disabled={isDisabled} className="bg-green-700 mb-4 font-semibold text-xl disabled:bg-slate-400 disabled:text-zinc-700 disabled:cursor-not-allowed rounded py-3 text-white cursor-pointer hover:bg-green-600 transition">
                    {text}
                </button>
            </Link>
        )
    } else {
        return (
            isWidthFull ? 
            <Link href={linkTo}>
                <button disabled={isDisabled} className="bg-blue-600 font-semibold text-xl disabled:bg-slate-400 disabled:text-zinc-700 disabled:cursor-auto rounded w-full py-3 text-white cursor-pointer hover:bg-blue-500 transition">
                     {text}
                </button>
            </Link>
            :
            <Link href={linkTo}>
                <button disabled={isDisabled} className="bg-blue-600 font-semibold text-xl rounded p-3 disabled:bg-slate-400 disabled:text-zinc-700 disabled:cursor-not-allowed text-white cursor-pointer hover:bg-blue-500 transition">
                     {text}
                </button>
            </Link>
        )
    }
}
