import { useState } from "react";
import { MdArticle, MdDashboard } from "react-icons/md"
import { BsFillCaretDownFill } from "react-icons/bs"
import { FiLogOut } from "react-icons/fi"
import Link from "next/link"
import { IoMdSettings } from "react-icons/io"

export const Navbar = () => {
    const [toggleUserOptions, setToggleUserOptions] = useState(false)


    const userLogged = 'Edvaldo'
    return(
        <nav className='absolute flex justify-between p-2 align-middle w-full h-16 bg-black top-0 left-0 text-white'>
            <div className='h-full flex flex-col align-middle justify-center'>
                <Link href='/home'>
                    <div className='flex justify-evenly align-middle'>
                        <MdArticle className='text-4xl mr-2' />
                        <h1 className='font-bold text-lg mt-1'>RLS Devsquad Articles</h1> 
                    </div>
                </Link>
            </div>
            <div className='h-full flex flex-col align-middle justify-center'>
                <div className='flex justify-evenly align-middle cursor-pointer'>
                    <div onClick={() => setToggleUserOptions(!toggleUserOptions)} className='flex justify-evenly align-middle cursor-pointer py-2'>
                        <h1 className='font-bold text-base select-none'>{`Hello again, ${userLogged}!`}</h1> 
                        <BsFillCaretDownFill className='text-xl ml-2 mr-2 mt-1' />
                    </div>
                    <ul className={toggleUserOptions ? 'absolute top-16 right-24 w-48 z-10 bg-white text-black rounded-b-md border-black border-2' : 'hidden'}>
                        <Link href='/dashboard'>
                            <li className='p-2 flex hover:text-white hover:bg-black transition select-none'><MdDashboard className='mt-1 mr-2' />Admin Dashboard</li>
                        </Link>
                        <div className="bg-zinc-900 h-px mx-3"></div>
                        <Link href='/settings'>
                            <li className='p-2 flex hover:text-white hover:bg-black transition select-none'><IoMdSettings className='mt-1 mr-2' />User settings</li>
                        </Link>
                    </ul>
                    <Link href='/login'>
                        <div className='flex justify-evenly align-middle cursor-pointer py-2'>
                            <h1 className='font-bold text-base'>Logout</h1> 
                            <FiLogOut className='text-xl ml-2 mt-1' />
                        </div>
                    </Link>
                </div>
            </div>
        </nav>
    )
}