import { useState } from "react";
import { FaCommentDots, FaEye, FaMusic, FaNewspaper, FaRobot, FaSave } from 'react-icons/fa'
import { MdSchool, MdWork } from 'react-icons/md'
import { BiFootball } from 'react-icons/bi'
import { IoLogoGameControllerB } from 'react-icons/io'
import { BsCaretDownFill, BsStar, BsStarFill, BsStarHalf } from 'react-icons/bs'
import { RichTextEditor } from './RichTextEditor'
import moment from 'moment'
import Link from 'next/link'

export enum Categories {
    news = 'News',
    sports = 'Sports',
    business = 'Business',
    education = 'Education',
    tech = 'Tech',
    pop = 'Pop',
    games = 'Games',
    music = 'Music',
    none = 'Select a category'
}
export interface IComments {
    userId: string
    comment: string
}

export type IArticleProps = {
    title: string
    category: Categories
    content?: string
    createdAt: string
    isArticleEmpty?: boolean
    rating: number
    comments: IComments[]
}

export function defineCategoryInfo(category: Categories) {
    let result = {
        baseClassNameReturn: 'absolute select-none flex border-b-2 border-l-2 border-r-2 py-1 rounded-b overflow px-4 top-0 right-2',
        iconCategory: (<></>)
    }
    if(category === Categories.news) {
        result.baseClassNameReturn += ' bg-red-500'
        result.iconCategory = <FaNewspaper className='mt-1 mr-2' />
    }
    if(category === Categories.business) {
        result.baseClassNameReturn += ' bg-blue-700'
        result.iconCategory = <MdWork className='mt-1 mr-2' />
    }
    if(category === Categories.tech) {
        result.baseClassNameReturn += ' bg-blue-500'
        result.iconCategory = <FaRobot className='mt-1 mr-2' />
    }
    if(category === Categories.music) {
        result.baseClassNameReturn += ' bg-purple-500'
        result.iconCategory = <FaMusic className='mt-1 mr-2' />
    }
    if(category === Categories.education) {
        result.baseClassNameReturn += ' bg-orange-500'
        result.iconCategory = <MdSchool className='mt-1 mr-2' />
    }
    if(category === Categories.sports) {
        result.baseClassNameReturn += ' bg-green-700'
        result.iconCategory = <BiFootball className='mt-1 mr-2' />
    }
    if(category === Categories.games) {
        result.baseClassNameReturn += ' bg-yellow-600'
        result.iconCategory = <IoLogoGameControllerB className='mt-1 mr-2' />
    }
    if(category === Categories.pop) {
        result.baseClassNameReturn += ' bg-pink-700'
        result.iconCategory = <BsStarFill className='mt-1 mr-2' />
    }
    if(category === Categories.none) {
        result.baseClassNameReturn += ' bg-gray-700 cursor-pointer w-48'
        result.iconCategory = <BsCaretDownFill className='mt-1 mr-2' />
    }

    return result
}

export function defineRating(rating: number | undefined) {
    if(!rating || rating === 0) {
        return(
            <div className="flex text-amber-600">
                <BsStar /><BsStar /><BsStar /><BsStar /><BsStar />
            </div>
            
        )
    }
    if(rating > 0 && rating <= 0.5) {
        return(
            <div className="flex text-amber-600">
                <BsStarHalf /><BsStar /><BsStar /><BsStar /><BsStar />
            </div>
            
        )
    }
    if(rating > 0.5 && rating <= 1) {
        return(
            <div className="flex text-amber-600">
                <BsStarFill /><BsStar /><BsStar /><BsStar /><BsStar />
            </div>
            
        )
    }
    if(rating > 1 && rating <= 1.5) {
        return(
            <div className="flex text-amber-600">
                <BsStarFill /><BsStarHalf /><BsStar /><BsStar /><BsStar />
            </div>
            
        )
    }
    if(rating > 1.5 && rating <= 2) {
        return(
            <div className="flex text-amber-600">
                <BsStarFill /><BsStarFill /><BsStar /><BsStar /><BsStar />
            </div>
            
        )
    }
    if(rating > 2 && rating <= 2.5) {
        return(
            <div className="flex text-amber-600">
                <BsStarFill /><BsStarFill /><BsStarHalf /><BsStar /><BsStar />
            </div>
            
        )
    }
    if(rating > 2.5 && rating <= 3) {
        return(
            <div className="flex text-amber-600">
                <BsStarFill /><BsStarFill /><BsStarFill /><BsStar /><BsStar />
            </div>
            
        )
    }
    if(rating > 3 && rating <= 3.5) {
        return(
            <div className="flex text-amber-600">
                <BsStarFill /><BsStarFill /><BsStarFill /><BsStarHalf /><BsStar />
            </div>
            
        )
    }
    if(rating > 3.5 && rating <= 4) {
        return(
            <div className="flex text-amber-600">
                <BsStarFill /><BsStarFill /><BsStarFill /><BsStarFill /><BsStar />
            </div>
            
        )
    }
    if(rating > 4 && rating <= 4.5) {
        return(
            <div className="flex text-amber-600">
                <BsStarFill /><BsStarFill /><BsStarFill /><BsStarFill /><BsStarHalf />
            </div>
            
        )
    }
    if(rating > 4.5 && rating <= 5) {
        return(
            <div className="flex text-amber-600">
                <BsStarFill /><BsStarFill /><BsStarFill /><BsStarFill /><BsStarFill />
            </div>
            
        )
    }
}

export function defineCountcomments(comments: IComments[]) {
    if(comments) {
        return {
            comments: comments[0].comment,
            count: comments.length
        }
    }

    return {
        comments: [''],
        count: 0
    }
}

export const Article = ({title, category, content, isArticleEmpty, rating, comments, createdAt}: IArticleProps) => {
    const [isButtonSaveArticleDisabled, setIsButtonSaveArticleDisabled] = useState(true)
    const [categorySelectedNewArticle,setCategorySelectedNewArticle] = useState(Categories.none)
    const [toggleCategorySelect, setToggleCategorySelect] = useState(false)

    const categoryInfo = defineCategoryInfo(category)
    const starsRating = defineRating(rating)
    const objcomments = defineCountcomments(comments) 
    if(isArticleEmpty) {
        return(
            <main className="relative flex flex-col justify-evenly align-middle bg-slate-400 box-content p-4 max-w-128 rounded my-4">
                <input type="text" placeholder={title} className='placeholder:text-zinc-600 text-3xl w-4/6 bg-transparent outline-none border-none mb-4 font-extrabold text-zinc-800 box-content'/>
                <div onClick={() => setToggleCategorySelect(!toggleCategorySelect)} className={categoryInfo.baseClassNameReturn}>
                    {categoryInfo.iconCategory}
                    <p>{category}</p>
                </div>
                    <ul className={toggleCategorySelect ? "absolute right-2 top-8 border-b-2 border-l-2 border-r-2 none rounded-b w-48 duration-700" : "duration-700 hidden"}>
                        <li onClick={() => setToggleCategorySelect(!toggleCategorySelect)} className="p-2 flex justify-start align-middle cursor-pointer transition-all bg-red-500 hover:bg-red-700"><FaNewspaper className="mt-1 mr-2" />News</li>
                        <li onClick={() => setToggleCategorySelect(!toggleCategorySelect)} className="p-2 flex justify-start align-middle cursor-pointer transition-all bg-green-700 hover:bg-green-900"><BiFootball className="mt-1 mr-2" />Sports</li>
                        <li onClick={() => setToggleCategorySelect(!toggleCategorySelect)} className="p-2 flex justify-start align-middle cursor-pointer transition-all bg-blue-700 hover:bg-blue-900"><MdWork className="mt-1 mr-2" />Business</li>
                        <li onClick={() => setToggleCategorySelect(!toggleCategorySelect)} className="p-2 flex justify-start align-middle cursor-pointer transition-all bg-orange-500 hover:bg-orange-700"><MdSchool className="mt-1 mr-2" />Education</li>
                        <li onClick={() => setToggleCategorySelect(!toggleCategorySelect)} className="p-2 flex justify-start align-middle cursor-pointer transition-all bg-blue-500 hover:bg-blue-700"><FaRobot className="mt-1 mr-2" />Tech</li>
                        <li onClick={() => setToggleCategorySelect(!toggleCategorySelect)} className="p-2 flex justify-start align-middle cursor-pointer transition-all bg-pink-700 hover:bg-pink-900"><BsStarFill className="mt-1 mr-2" />Pop</li>
                        <li onClick={() => setToggleCategorySelect(!toggleCategorySelect)} className="p-2 flex justify-start align-middle cursor-pointer transition-all bg-yellow-600 hover:bg-yellow-900"><IoLogoGameControllerB className="mt-1 mr-2" />Games</li>
                        <li onClick={() => setToggleCategorySelect(!toggleCategorySelect)} className="p-2 flex justify-start align-middle cursor-pointer transition-all bg-purple-500 hover:bg-purple-700"><FaMusic className="mt-1 mr-2" />Music</li> 
                    </ul>
                <div className='w-full flex justify-end'>
                    <p className='text-sm text-zinc-900'>{`Today, ${moment().format('LLL')}`}</p>
                </div>
                <RichTextEditor />
                <div className='flex justify-between align-middle mt-2'>
                    <div>
                        <p className='text-sm font-semibold text-zinc-800'><strong>By</strong> Edvaldo de Ramos Junior</p>
                    </div>
                    <button disabled={isButtonSaveArticleDisabled} className='flex justify-evenly align-middle cursor-pointer px-8 py-1 disabled:cursor-not-allowed disabled:bg-slate-600/50 disabled:text-gray-900/50 bg-green-600 rounded'>
                        <FaSave className='mt-1 mr-2' />
                        <p>Save</p>
                    </button>
                </div>
            </main>
        )
    }
    return(
        <main className="relative bg-slate-400 box-content text-sm p-4 max-w-128 rounded my-4">
            <div className='absolute top-3 left-4 flex justify-evenly align-middle text-zinc-700'>
                <div className='flex text-sm'>
                    <p className='mx-1 text-amber-700 font-bold'>{String(rating.toFixed(1))}</p>
                    <div className='flex ml-1 mr-1 mt-1'>
                        {starsRating}
                    </div>
                </div>
                <div className='flex text-sm font-bold'>
                    <FaCommentDots className='ml-2 mr-1 mt-1' />
                    <p>{String(objcomments.count).padStart(2,'0')} comments</p>
                </div>
            </div>
            <h1 className='text-4xl mt-6 mb-4 font-extrabold text-zinc-800 box-content'>{title}</h1>
            <div className={categoryInfo.baseClassNameReturn}>
                {categoryInfo.iconCategory}
                <p>{category}</p>
            </div>
            <div className='w-full flex justify-end'>
                    <p className='text-sm mb-1 text-zinc-900'>{moment(createdAt).format('LLLL')}</p>
            </div>
            <p className="text-zinc-950 bg-zinc-300/70 mb-4 p-4 text-base border-solid rounded border">{content}</p>
            <div className='flex justify-between align-middle pb-2'>
                <div>
                    <p className='text-sm font-semibold text-zinc-800'><strong>By</strong> Edvaldo de Ramos Junior</p>
                </div>
                <Link href='/article-detail'>
                    <button className='flex justify-evenly align-middle px-8 py-1 bg-purple-800 rounded'>
                        <FaEye className='mt-1 mr-2' />
                        <p>See more</p>
                    </button>
                </Link>
            </div>
        </main>
    )
}