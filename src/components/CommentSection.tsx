import { FaUserCircle } from "react-icons/fa";
import { IComments } from "./Article"

type IProps = {
    comments: IComments[]
}

function generateCommentsList(comments: IComments[]) {
    for(const comment of comments) {
        return(
            <div className="w-full p-4 bg-white rounded-md flex flex-col justify-start align-middle">
                <h4 className="text-black font-bold text-lg mb-2">Edvaldo de Ramos Junior</h4>
                <p className="text-zinc-900">{comment.comment}</p>
            </div>
        )        
    }
}

export const CommentSection = ({comments}:IProps) => {
    const commentsList = generateCommentsList(comments)
    if(!comments) {
        return(
            <main>
                <h1 className='text-4xl font-extrabold text-zinc-900'>Comments (0)</h1>
            </main>
        )
    }
    return(
        <main>
            <h1 className='text-4xl font-extrabold text-zinc-900'>Comments {`(${String(comments.length).padStart(2,'0')})`}</h1>
            <div className='flex justify-evenly align-middle w-full my-4'>
                <FaUserCircle className='text-7xl mr-3 text-zinc-950' />
                {commentsList}
            </div>
        </main>
    )
} 