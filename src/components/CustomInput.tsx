type IProps =  React.DetailedHTMLProps<React.InputHTMLAttributes<HTMLInputElement>, HTMLInputElement> & {
    label?: string,
    className: string
}

export const CustomInput = ({className, label, ...inputProps}: IProps) => {
    if(inputProps.type === 'text' || inputProps.type === 'email' || inputProps.type === 'password') {
        return(
            <>
                <label className="my-2">{label}</label>
                    <input 
                        {...inputProps}
                        className={className}
                    />
            </>
        )
    }
    return(<></>)
}