import { FaCommentDots,FaMusic,FaNewspaper,FaRobot } from "react-icons/fa";
import { Categories, IArticleProps, defineCountcomments, defineRating } from "./Article";
import moment from "moment";
import { MdSchool,MdWork } from "react-icons/md";
import { BiFootball } from "react-icons/bi";
import { IoLogoGameControllerB, IoMdSend } from "react-icons/io";
import { BsStar,BsStarFill } from "react-icons/bs";
import { CustomInput } from "./CustomInput";
import { CommentSection } from "./CommentSection";


function defineCategoryInfo(category: Categories) {
    let result = {
        baseClassNameReturn: 'absolute select-none text-xl flex border-b-2 border-l-2 border-r-2 py-1 rounded-b overflow px-6 top-0 right-3',
        iconCategory: (<></>)
    }
    if(category === Categories.news) {
        result.baseClassNameReturn += ' bg-red-500'
        result.iconCategory = <FaNewspaper className='mt-1 mr-2' />
    }
    if(category === Categories.business) {
        result.baseClassNameReturn += ' bg-blue-700'
        result.iconCategory = <MdWork className='mt-1 mr-2' />
    }
    if(category === Categories.tech) {
        result.baseClassNameReturn += ' bg-blue-500'
        result.iconCategory = <FaRobot className='mt-1 mr-2' />
    }
    if(category === Categories.music) {
        result.baseClassNameReturn += ' bg-purple-500'
        result.iconCategory = <FaMusic className='mt-1 mr-2' />
    }
    if(category === Categories.education) {
        result.baseClassNameReturn += ' bg-orange-500'
        result.iconCategory = <MdSchool className='mt-1 mr-2' />
    }
    if(category === Categories.sports) {
        result.baseClassNameReturn += ' bg-green-700'
        result.iconCategory = <BiFootball className='mt-1 mr-2' />
    }
    if(category === Categories.games) {
        result.baseClassNameReturn += ' bg-yellow-600'
        result.iconCategory = <IoLogoGameControllerB className='mt-1 mr-2' />
    }
    if(category === Categories.pop) {
        result.baseClassNameReturn += ' bg-pink-700'
        result.iconCategory = <BsStarFill className='mt-1 mr-2' />
    }

    return result
}

export const FullArticle = ({title, category, content, rating, comments, createdAt}: IArticleProps) => {
    const categoryInfo = defineCategoryInfo(category)
    const starsRating = defineRating(rating)
    const objcomments = defineCountcomments(comments) 
    const commentsList = [
        {
            userId:'', 
            comment:'Lorem ipsum dolor sit, amet consectetur adipisicing elit. Fuga porro consectetur, veritatis error vero, beatae sapiente aut velit aliquam cumque facere. Illo perferendis ducimus deserunt optio ab. Commodi similique natus praesentium quia nostrum consequatur cupiditate facere itaque accusamus temporibus a quasi tempore aperiam excepturi, est, quis totam, fuga placeat ex recusandae. Distinctio atque at voluptatem corrupti omnis, possimus placeat, nam debitis molestias sed nemo ad, rem exercitationem iusto dolores ducimus quod consequatur architecto illum eum. Totam optio ex doloribus iusto a, porro earum mollitia fugiat. Quod voluptatibus nulla corrupti labore repellat dolor laudantium nostrum ea est fuga voluptates, dolorem perspiciatis!'
        }
    ]
    return(
        <main className="relative bg-slate-400 box-content text-sm p-4 mt-24 mb-8 rounded mx-8">
            <div className='absolute top-3 left-4 flex justify-evenly align-middle text-zinc-700'>
                <div className='flex text-xl'>
                    <p className='mx-1 text-amber-700 font-bold'>{String(rating.toFixed(1))}</p>
                    <div className='flex ml-1 mr-1 mt-1'>
                        {starsRating}
                    </div>
                </div>
                <div className='flex text-xl font-bold'>
                    <FaCommentDots className='ml-2 mr-1 mt-1' />
                    <p>{String(objcomments.count).padStart(2,'0')} comments</p>
                </div>
            </div>
            <h1 className='text-6xl mt-10 mb-4 font-extrabold text-zinc-800 box-content'>{title}</h1>
            <div className={categoryInfo.baseClassNameReturn}>
                {categoryInfo.iconCategory}
                <p>{category}</p>
            </div>
            <div className='w-full flex justify-end'>
                    <p className='text-sm mb-1 text-zinc-900'>{moment(createdAt).format('LLLL')}</p>
            </div>
            <p className="text-zinc-950 bg-zinc-300/70 mb-4 p-4 text-base border-solid rounded border">{content}</p>
            <div className='flex justify-between align-middle pb-2'>
                <div>
                    <p className='text-sm font-semibold text-zinc-800'><strong>By</strong> Edvaldo de Ramos Junior</p>
                </div>
            </div>
            <div className='mb-6'>
                <h1 className="text-3xl my-6 font-extrabold text-zinc-900 justify-center flex">Tell us, what do you this article?</h1>
                <div className="flex justify-evenly text-3xl text-amber-700">
                    <BsStar /><BsStar /><BsStar /><BsStar /><BsStar />
                </div>
            </div>
                <div className='flex justify-center align-middle mb-6'>
                    <CustomInput 
                        type='text'
                        id='username-input'
                        placeholder='Leave a message here..'
                        className='bg-slate-800 rounded text-lg py-2 px-3 outline-none w-full text-white'
                    />
                    <button title='Send message' className="flex justify-center cursor:pointer align-middle p-4 rounded-md text-lg ml-2 bg-emerald-600">
                        <IoMdSend />
                    </button>
                </div>
                <CommentSection 
                    comments={commentsList}
                />
        </main>
    )
}