import './styles/globals.css'
import type { Metadata } from 'next'

export const metadata: Metadata = {
  title: 'RLS Devsquad - Article Platform',
  description: 'Platform to read articles and interact with all peoples',
}

export default function RootLayout({
  children,
}: {
  children: React.ReactNode
}) {
  return (
    <html lang="en">
      <body>{children}</body>
    </html>
  )
}
