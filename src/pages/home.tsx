import { Article, Categories } from "@/components/Article";
import { Navbar } from "@/components/Navbar";
import Head from "next/head";

export default function Home() {
    return(
        <>
            <Head>
                <title>RLS Devsquad - Home</title>
                <meta property="og:title" content="RLS Devsquad - Home" key="title" />
            </Head>
            <main className="flex flex-col min-h-screen justify-center align-middle overflow-hidden pb-4 pt-10 bg-zinc-900 text-zinc-50">
                <Navbar />
                <div className="mt-10 px-10 flex flex-col justify-center align-middle">
                    <Article 
                        isArticleEmpty={true}
                        title="Type your title here..."
                        category={Categories.none}
                        rating={0}
                        comments={[{userId:'',comment:''}]}
                        createdAt=""
                        />
                <h1 className="my-3 text-white text-8xl font-extrabold">Articles</h1>
                    <Article 
                        title="To Navigate the Age of AI, the World Needs a New Turing Test"
                        category={Categories.tech}
                        content='There was a time in the not too distant past—say, nine months ago—when the Turing test seemed like a pretty stringent detector of machine 
                        intelligence. Chances are youre familiar with how it works: Human judges hold text conversations with two hidden interlocutors, one human and one 
                        computer, and try to determine which is which. If the computer manages to fool at least 30 percent of the judges, it passes the test and is pronounced 
                        capable of thought...'
                        rating={2.1}
                        comments={[{userId:'',comment:''},{userId:'',comment:''},{userId:'',comment:''}]}
                        createdAt="2023-08-01T12:59:10.371Z"
                        />
                    <Article 
                        title="Dwyane Wade pays homage to idol Allen Iverson"
                        category={Categories.sports}
                        content='Dwyane Wade picked Allen Iverson to be his Hall of Fame presenter for a special reason. When Wade delivered his Hall of Fame speech on Saturday evening, he made 
                        sure to pay tribute to the player who inspired him growing up. The Miami Heat legend spoke of Iversons impact on his basketball career, reiterating that 
                        Iverson was his hero as a kid growing up in Chicago.Our heroes are not always perfect. Instead, they possess a relatability that makes them touchable and real. 
                        When anyone speaks about Allen Iverson, thats exactly what they say, said Wade...'
                        rating={4}
                        comments={[{userId:'',comment:''}]}
                        createdAt="2023-02-10T20:55:08.371Z"
                        />
                    <Article 
                        title="Busta Rhymes credits hip-hop for helping him to become a man"
                        category={Categories.news}
                        content='Busta Rhymes has explained how hip-hop and becoming a father helped him become a man.
                        During an interview with Mens Health, the 51-year-old rapper opened up about how hip-hop, as well as becoming a father, forced 
                        him into becoming a man.
                        I was the youngest in Leaders to have a child, the rapper said, referring to the New York-based hip-hop group Leaders of the 
                        New School, which he joined in 1986. That forced me to understand the seriousness of what becoming a man was...'
                        rating={1.5}
                        comments={[{userId:'',comment:''},{userId:'',comment:''},{userId:'',comment:''},{userId:'',comment:''},{userId:'',comment:''},{userId:'',comment:''}]}
                        createdAt="2023-05-22T08:32:00.371Z"
                        />
                </div>
            </main>
        </>
    )
}