import { Categories } from "@/components/Article";
import { FullArticle } from "@/components/FullArticle";
import { Navbar } from "@/components/Navbar";
import Head from "next/head";

export default function ArticleDetailPage() {
    return(
        <>
            <Head>
                <title>RLS Devsquad - Article Detail</title>
                <meta property="og:title" content="RLS Devsquad - Article Detail" key="title" />
            </Head>
            <main className="flex flex-col min-h-screen justify-start align-middle bg-zinc-900 text-zinc-50">
                <Navbar />
                <FullArticle
                        title="To Navigate the Age of AI, the World Needs a New Turing Test"
                        category={Categories.tech}
                        content='There was a time in the not too distant past—say, nine months ago—when the Turing test seemed like a pretty stringent detector of machine 
                        intelligence. Chances are youre familiar with how it works: Human judges hold text conversations with two hidden interlocutors, one human and one 
                        computer, and try to determine which is which. If the computer manages to fool at least 30 percent of the judges, it passes the test and is pronounced 
                        capable of thought.
                        For 70 years, it was hard to imagine how a computer could pass the test without possessing what AI researchers now call artificial general intelligence, 
                        the entire range of human intellectual capacities. Then along came large language models such as GPT and Bard, and the Turing test suddenly began seeming 
                        strangely outmoded. OK, sure, a casual user today might admit with a shrug, GPT-4 might very well pass a Turing test if you asked it to impersonate a human. 
                        But so what? LLMs lack long-term memory, the capacity to form relationships, and a litany of other human capabilities. They clearly have some way to go before 
                        were ready to start befriending them, hiring them, and electing them to public office.
                        And yeah, maybe the test does feel a little empty now. But it was never merely a pass/fail benchmark. Its creator, Alan Turing, a gay man sentenced in his time to 
                        chemical castration, based his test on an ethos of radical inclusivity: The gap between genuine intelligence and a fully convincing imitation of intelligence is only 
                        as wide as our own prejudice. When a computer provokes real human responses in us—engaging our intellect, our amazement, our gratitude, our empathy, even our fear—that 
                        is more than empty mimicry.
                        So maybe we need a new test: the Actual Alan Turing Test. Bring the historical Alan Turing, father of modern computing—a tall, fit, somewhat awkward man with straight 
                        dark hair, loved by colleagues for his childlike curiosity and playful humor, personally responsible for saving an estimated 14 million lives in World War II by 
                        cracking the Nazi Enigma code, subsequently persecuted so severely by England for his homosexuality that it may have led to his suicide—into a comfortable laboratory 
                        room with an open MacBook sitting on the desk. Explain that what he sees before him is merely an enormously glorified incarnation of what is now widely known by computer 
                        scientists as a “Turing machine.” Give him a second or two to really take that in, maybe offering a word of thanks for completely transforming our world. Then hand him a 
                        stack of research papers on artificial neural networks and LLMs, give him access to GPTs source code, open up a ChatGPT prompt window—or, better yet, 
                        a Bing-before-all-the-sanitizing window—and set him loose.
                        Imagine Alan Turing initiating a light conversation about long-distance running, World War II historiography, and the theory of computation. Imagine him seeing the realization 
                        of all his wildest, most ridiculed speculations scrolling with uncanny speed down the screen. Imagine him asking GPT to solve elementary calculus problems, to infer what human 
                        beings might be thinking in various real-world scenarios, to explore complex moral dilemmas, to offer marital counseling and legal advice and an argument for the possibility of 
                        machine consciousness—skills which, you inform Turing, have all emerged spontaneously in GPT without any explicit direction by its creators. Imagine him experiencing that little 
                        cognitive-emotional lurch that so many of us have now felt: Hello, other mind.'
                        rating={2.1}
                        comments={[{userId:'',comment:''},{userId:'',comment:''},{userId:'',comment:''}]}
                        createdAt="2023-08-01T12:59:10.371Z"
                />
            </main>
        </>
    )
}