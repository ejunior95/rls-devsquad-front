import { MdArticle } from 'react-icons/md';
import Head from 'next/head'
import { CustomButtom } from '@/components/CustomButton';
import { CustomInput } from '@/components/CustomInput';

export default function Login() {
    return(
        <>
            <Head>
                <title>RLS Devsquad Articles - Login</title>
                <meta property="og:title" content="RLS Devsquad - Login" key="title" />
            </Head>
            <main className="relative flex flex-col place-items-center min-h-screen align-middle justify-center overflow-hidden bg-zinc-950">
                <div className="bg-zinc-100 flex flex-col align-middle px-20 pt-10 pb-8 rounded w-3/6 max-w-2xl">
                    <div>
                        <div className='flex justify-center align-middle'>
                            <MdArticle className='text-black text-5xl' />
                            <h1 className='text-black font-bold text-3xl mt-1 ml-2'>RLS Devsquad Articles</h1> 
                        </div>
                        <p className='w-full text-center text-zinc-900'>
                            Content platform that empowers our content team to create and publish articles
                        </p>
                    </div>
                    <form className='flex justify-center align-middle flex-col mb-10'>
                        <CustomInput 
                            label='Username'
                            type='email'
                            id='username-input'
                            placeholder='Type your email here...'
                            className='bg-slate-800 rounded text-sm py-2 px-3 outline-none w-full text-white'
                            
                        />
                        <CustomInput 
                            label='Password'
                            type='password'
                            id='username-password'
                            placeholder='Type your password here...'
                            className='bg-slate-800 rounded text-sm py-2 px-3 outline-none w-full text-white'
                        />
                    </form>
                    <div>
                        <CustomButtom
                            linkTo='/home'
                            type='primary'
                            text='Login'
                            isWidthFull={true}
                        />
                        <div className="flex text-sm justify-center mb-2">
                            <p className="mx-1">New to Devsquad Articles?</p>
                            <p className="text-blue-800 font-medium text-center">Create an account</p>
                        </div>
                        <CustomButtom 
                            linkTo='/register'
                            type='secondary'
                            text='Register now'
                            isWidthFull={true}
                        />
                    </div>
                </div>
            </main>
        </>
    )
}