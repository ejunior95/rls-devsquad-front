import { CustomButtom } from "@/components/CustomButton";
import { CustomInput } from "@/components/CustomInput";
import Head from "next/head";
import Link from "next/link";

export default function Register() {
    return(
        <>
            <Head>
                <title>RLS Devsquad Articles - Register</title>
                <meta property="og:title" content="RLS Devsquad - Register" key="title" />
            </Head>
            <main className="relative flex flex-col place-items-center min-h-screen align-middle overflow-hidden justify-center bg-zinc-950">
                <div className="bg-zinc-100 flex flex-col align-middle px-10 pt-2 rounded w-3/6 max-w-2xl">
                    <div className="mt-4 mb-4">
                        <h1 className='text-black font-bold text-3xl w-full text-center'>Register now</h1> 
                        <p className='w-full text-center text-zinc-900'>
                            Enter your details below to access the platform
                        </p>
                    </div>
                    <form className='flex justify-center align-middle pt-14 mt-4 mb-6 flex-col h-80 overflow-y-scroll'>
                        <CustomInput 
                            label='Please, tell us your name'
                            type='text'
                            id='name-input'
                            placeholder='Your name here...'
                            className='bg-slate-800 rounded text-sm py-2 px-3 outline-none w-full text-white'
                        />
                        <CustomInput 
                            label='Enter with your email'
                            type='email'
                            id='username-input'
                            placeholder='Type your email here...'
                            className='bg-slate-800 rounded text-sm py-2 px-3 outline-none w-full text-white'
                        />
                        <CustomInput 
                            label='Please, enter again with your email'
                            type='email'
                            id='confirm-username-input'
                            placeholder='Confirm your email here...'
                            className='bg-slate-800 rounded text-sm py-2 px-3 outline-none w-full text-white'
                        />
                        <CustomInput 
                            label='Enter with a password'
                            type='password'
                            id='username-password'
                            placeholder='Type your password here...'
                            className='bg-slate-800 rounded text-sm py-2 px-3 outline-none w-full text-white'
                        />
                        <CustomInput 
                            label='Please, enter again with your password'
                            type='password'
                            id='confirm-username-password'
                            placeholder='Confirm your password here...'
                            className='bg-slate-800 rounded text-sm py-2 px-3 outline-none w-full text-white'
                        />
                    </form>
                    <div>
                        <CustomButtom 
                            linkTo='/home'
                            type='secondary'
                            text='Save'
                            isWidthFull={true}
                            isDisabled={true}
                        />

                        <div className="flex text-sm justify-center mb-2">
                            <p className="mx-1">Already have an account?</p>
                            <Link href="/login">
                                <p className="text-blue-800 font-medium cursor-pointer text-center">Sign in!</p>
                            </Link>
                        </div>
                    </div>
                </div>
            </main>
        </>
    )
}