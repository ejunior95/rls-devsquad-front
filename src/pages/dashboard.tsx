import { Navbar } from "@/components/Navbar";
import Head from "next/head";

export default function Dashboard() {
    return(
        <>
            <Head>
                <title>RLS Devsquad - Admin Dashboard</title>
                <meta property="og:title" content="RLS Devsquad - Admin Dashboard" key="title" />
            </Head>
            <main className="flex min-h-screen justify-center overflow-hidden bg-zinc-950 text-zinc-50">
                <Navbar />
            </main>
        </>
    )
}